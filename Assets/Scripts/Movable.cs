﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movable : MonoBehaviour
{
    public bool isPickedUp = false;
    public bool isNearSnapPosition = false;
    int environmentMask;

    [SerializeField]
    MeshRenderer snapPreviewMesh;

    [SerializeField]
    Material hoverMaterial;


    Vector3 snapPoint; // the point on the ground where the tower will snap to
    Quaternion snapRot;
    MeshRenderer meshRenderer; // for switching materials


    bool isInPickupRange = false;

    [SerializeField]
    Transform trackingSpace;

    OVRInput.Controller currHand;

    Material[] defaultMaterials;

    // Start is called before the first frame update
    void Start()
    {
        meshRenderer = transform.GetComponent<MeshRenderer>();
        defaultMaterials = new Material[meshRenderer.materials.Length];
        for (int i = 0; i < meshRenderer.materials.Length; i++) {
            defaultMaterials[i] = meshRenderer.materials[i];
        }
        trackingSpace = GameObject.Find("TrackingSpace").transform;
        environmentMask = LayerMask.GetMask("Teleport Environment");
    }

    // Update is called once per frame
    void Update() {
        if (isPickedUp) {
            MoveObject();
        }
        if (OVRInput.GetDown(OVRInput.RawButton.RHandTrigger)) {
            if (isInPickupRange) {
                isPickedUp = true;
                StartCoroutine(ChangeScale(new Vector3(0.2f, 0.2f, 0.2f)));
                currHand = OVRInput.Controller.RHand;
            }
        } else if (OVRInput.GetDown(OVRInput.RawButton.LHandTrigger)) {
            if (isInPickupRange) {
                isPickedUp = true;
                StartCoroutine(ChangeScale(new Vector3(0.2f, 0.2f, 0.2f)));
                currHand = OVRInput.Controller.LHand;
            }
        } else if ((currHand == OVRInput.Controller.LHand && OVRInput.GetUp(OVRInput.RawButton.LHandTrigger) ||
            (currHand == OVRInput.Controller.RHand && OVRInput.GetUp(OVRInput.RawButton.RHandTrigger)))) {
            isPickedUp = false;
            StartCoroutine(ChangeScale(new Vector3(1f, 1f, 1f)));
            if (isNearSnapPosition) {
                snapPreviewMesh.enabled = false;
                ShowTransferPoints();
                StartCoroutine(LerpToSnap());
            }
        }


    }

    void ChangeToHoverMaterial() {
        for (int i = 0; i < meshRenderer.materials.Length; i++) {
            meshRenderer.materials[i] = hoverMaterial;
            meshRenderer.material = hoverMaterial;
        }
    }

    void ResetMaterial() {
        meshRenderer.materials = defaultMaterials;
    }

    IEnumerator ChangeScale(Vector3 size) {
        float timeLeft = 0.2f;
        var positionLerpPct = 1f - Mathf.Exp((Mathf.Log(1f - 0.99f) / timeLeft) * Time.deltaTime);
        while (timeLeft > 0f) {
            timeLeft -= Time.deltaTime;
            transform.localScale = Vector3.Lerp(transform.localScale, size, positionLerpPct);
            yield return new WaitForSeconds(0.01f);
        }
        transform.localScale = size;
    }


    void MoveObject() {
        Vector3 handPosition = trackingSpace.TransformPoint(OVRInput.GetLocalControllerPosition(currHand));
        transform.position = handPosition;
    }

    void MouseClickMove() {
        RaycastHit hit;
        // Only hit things w/ teleport environment mask
        if (!Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100f, environmentMask)) {
            return;
        }
        transform.position = Vector3.Lerp(transform.position, hit.point + new Vector3(0f, 0.3f, 0f), 0.2f);
    }

    private void OnMouseDown() {
        isPickedUp = true;
    }

    private void OnMouseUp() {
        isPickedUp = false;
        if (isNearSnapPosition) {
            snapPreviewMesh.enabled = false;
            ShowTransferPoints();
            StartCoroutine(LerpToSnap());
        }
    }

    void ShowTransferPoints() {
        GameObject[] dropPoints = GameObject.FindGameObjectsWithTag("Tower Drop Off Point");
        foreach (GameObject dropPoint in dropPoints) {
            if (Vector3.Distance(snapPoint, dropPoint.transform.position) < 10f) {
                dropPoint.transform.localScale = new Vector3(3, 3, 3);
            }
        }
    }

    private void OnTriggerStay(Collider other) {
        if (other.tag.Equals("Player")) {
            isInPickupRange = true;
            Debug.Log("Test");
            ChangeToHoverMaterial();
        } else if (isPickedUp && other.transform.tag.Equals("Snap Point")) {
            isNearSnapPosition = true;
            snapPoint = other.transform.position + new Vector3(-0.5f, 0f, 0f); // TODO to offset for off center mesh
            snapRot = (other.transform.rotation);
            snapPreviewMesh.transform.position = snapPoint + new Vector3(0, 0.1f, 0f);
            snapPreviewMesh.transform.rotation = snapRot;
            snapPreviewMesh.enabled = true;
        }
    }

    private void OnTriggerExit(Collider other) {
        if (isNearSnapPosition) {
            isNearSnapPosition = false;
            snapPreviewMesh.enabled = false;
        }
        if (other.tag.Equals("Player")) {
            Debug.Log("Trigger Exit");
            isInPickupRange = false;
            ResetMaterial();
        }
    }

    IEnumerator LerpToSnap() {
    float timeLeft = 0.2f;
    var positionLerpPct = 1f - Mathf.Exp((Mathf.Log(1f - 0.99f) / timeLeft) * Time.deltaTime);

    while (timeLeft > 0f) {
        timeLeft -= Time.deltaTime;
        transform.position = Vector3.Lerp(transform.position, snapPoint, positionLerpPct);
        transform.rotation = Quaternion.Lerp(transform.rotation, snapRot, positionLerpPct);
        yield return new WaitForSeconds(0.01f);
    }
    transform.position = snapPoint;

    }



}
