﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MultipleChoiceAnswer : MonoBehaviour, IPointerUpHandler, IPointerDownHandler {
    Animator animator;
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnMouseOver() {
        Debug.Log("asd");
    }

    void IPointerDownHandler.OnPointerDown(PointerEventData eventData) {
        animator.SetBool("isPressed", true);
    }
    void IPointerUpHandler.OnPointerUp(PointerEventData eventData) {
        animator.SetBool("isPressed", false);
    }
}
