﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportController : MonoBehaviour
{

    [Range(1f, 1000f)]
    public float maxRaycastDistance = 100f;
    [Range(1, 20)]
    public int curveSegmentCount = 5;
    [SerializeField]
    Transform teleportIndicator;
    [SerializeField]
    Transform trackingSpace;

    [SerializeField]
    Transform leftHandAnchor;

    [SerializeField]
    Transform rightHandAnchor;


    LineRenderer lineRenderer;
    Vector3 teleportPos;
    Touch touch;
    int environmentMask;
    Animator teleportAnimator;
    bool isTeleporting;

    // Start is called before the first frame update
    void Start()
    {
        lineRenderer = GetComponentInChildren<LineRenderer>();
        lineRenderer.positionCount = curveSegmentCount;
        isTeleporting = false;
        environmentMask = LayerMask.GetMask("Teleport Environment");
        teleportPos = Vector3.negativeInfinity;
        teleportAnimator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isTeleporting)
        {
            return;
        }

        if (OVRInput.Get(OVRInput.RawButton.A)) {
            ShowTeleportLocation(OVRInput.Controller.RHand);
        } else if (OVRInput.Get(OVRInput.RawButton.X)) {
            ShowTeleportLocation(OVRInput.Controller.LHand);
        }

        if (OVRInput.GetUp(OVRInput.RawButton.A) || OVRInput.GetUp(OVRInput.RawButton.X))
        {
            TeleportToPoint();
        }
    }

    void ShowTeleportLocation(OVRInput.Controller hand)
    {

        teleportIndicator.gameObject.SetActive(true);

        Vector3 handPosition = trackingSpace.TransformPoint(OVRInput.GetLocalControllerPosition(hand));
        Vector3 handRotation;
        if (hand.Equals(OVRInput.Controller.LHand))
        {
            handRotation = leftHandAnchor.forward;
        } else
        {
            handRotation = rightHandAnchor.forward;
        }



        Ray ray = new Ray(handPosition, handRotation);
        RaycastHit hit;
        // Only hit things w/ teleport environment mask
        if (!Physics.Raycast(ray, out hit, maxRaycastDistance, environmentMask))
        {
            teleportPos = Vector3.negativeInfinity;
            teleportIndicator.gameObject.SetActive(false);
            return;
        }

        teleportPos = hit.point;
        teleportIndicator.position = new Vector3(hit.point.x, hit.point.y + 0.05f, hit.point.z); // Adding a small value to y to reduce clipping

        Quaternion rotateTarget = Quaternion.LookRotation(Vector3.Cross(teleportIndicator.right, hit.normal));
        teleportIndicator.rotation = Quaternion.Lerp(teleportIndicator.rotation, rotateTarget, 0.2f); // TODO adjust lerp value

        // TODO allow for y rotation (side by size) corresponding to controller orientation
        teleportIndicator.rotation = Quaternion.Euler(teleportIndicator.eulerAngles.x, handRotation.y, teleportIndicator.eulerAngles.z); // Changing the y rotation to face the same direction as the camera

        DrawCurve(handPosition);
    }

    // Draws a quadratic bezier curve from the player to the teleport position
    void DrawCurve(Vector3 handPos)
    {
        lineRenderer.enabled = true;
        lineRenderer.positionCount = curveSegmentCount;
        Vector3 startPoint = handPos; // TODO this has to be at the hand position
        Vector3 controlPoint = Vector3.Lerp(transform.position, teleportPos, 0.5f);
        controlPoint.y += 2f; // Arbitrary height on the curve

        lineRenderer.SetPosition(0, handPos);
        for (int i = 1; i < lineRenderer.positionCount - 1; i++)
        {
            float t = (float)i / curveSegmentCount;
            Vector3 q0 = Vector3.Lerp(startPoint, controlPoint, t);
            Vector3 q1 = Vector3.Lerp(controlPoint, teleportPos, t);
            Vector3 q2 = Vector3.Lerp(q0, q1, t);
            lineRenderer.SetPosition(i, q2);

            // Check intersection 


        }
        lineRenderer.SetPosition(lineRenderer.positionCount - 1, teleportPos);
    }

    // Teleports the camera's GameObject to the defined teleport destination
    void TeleportToPoint()
    {
        if (teleportPos.Equals(Vector3.negativeInfinity))
        {
            return;
        }
        lineRenderer.enabled = false;
        StartCoroutine(TeleportTransition());
    }

    IEnumerator TeleportTransition()
    {
        teleportAnimator.SetTrigger("isTeleportingTrigger");
        isTeleporting = true;
        yield return new WaitForSeconds(0f); // TODO currently nothing
        isTeleporting = false;
        transform.position = teleportPos;
        transform.Translate(0, 1.5f, 0); // TODO temp adjustment for player default height
    }


}
