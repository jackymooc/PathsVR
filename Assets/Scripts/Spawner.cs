﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Spawner : MonoBehaviour
{

    [Range(0.5f, 5f)]
    float spawnTime = 2f;
    [Range(0, 50)]
    int maxSpawnees = 10;
    [SerializeField]
    List<GameObject> prefabs;
    [SerializeField]
    Transform SpawneeParent;

    [SerializeField]
    Transform tempDest;

    List<Transform> spawnTransforms;
    int spawnCount = 0;

    WaitForSeconds spawnDelay;

    private void Awake() {
        spawnTransforms = new List<Transform>();
        for(int i = 0; i < transform.childCount; i++) {
            spawnTransforms.Add(transform.GetChild(i).GetComponent<Transform>());
        }
    }

    void Start()
    {
        spawnDelay = new WaitForSeconds(spawnTime);
        StartCoroutine(Spawn());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator Spawn() {

        while(true) {
            if (spawnCount <= maxSpawnees) {
                int randPrefabIndex = Random.Range(0, prefabs.Count);
                int rndLocationIndex = Random.Range(0, spawnTransforms.Count);
                //prefabs[randPrefabIndex].GetComponent<NavMeshAgent>().SetDestination(tempDest.position);
                Instantiate(prefabs[randPrefabIndex], spawnTransforms[rndLocationIndex].position, Quaternion.identity, SpawneeParent);
            }
            yield return spawnDelay;
        }
    }
}
