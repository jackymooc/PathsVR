﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClayPerson : MonoBehaviour
{

    Vector3 initialPos;
    Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        initialPos = transform.position;
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    Vector3 velocity;
    private void FixedUpdate() {
        //transform.position = transform.position + velocity * 0.01f;
    }

    private void OnCollisionStay(Collision collision) {
        Collider collider = collision.collider;
        if (collider.transform.tag.Equals("Conveyor")) {
            //rb.AddForce(collider.transform.forward * 10f, ForceMode.Force);
            rb.velocity = collision.transform.forward;
            //velocity = collider.transform.forward;
        }

        if(collider.transform.tag.Equals("Ramp")) {
            Debug.Log("MOVING UP RAMP");
            velocity = -collider.transform.right * 30f;
            velocity.y = 20f;
            rb.AddForce(velocity);
            //StartCoroutine(MoveUpRamp());
        }

    }

    IEnumerator MoveUpRamp() {
        rb.AddForce(velocity, ForceMode.Acceleration);
        yield return new WaitForSeconds(0.1f);

    }

    public void ResetPosition() {
        transform.position = initialPos;
        rb.velocity = Vector3.zero;
    }

}
