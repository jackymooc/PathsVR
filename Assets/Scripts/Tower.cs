﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{

    Movable tower;
    SphereCollider sphereCollider; // triggers when clay people enter

    // Start is called before the first frame update
    void Start()
    {
        sphereCollider = transform.GetComponent<SphereCollider>();
        tower = transform.GetComponent<Movable>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseDown() {
        sphereCollider.enabled = true;
    }

    private void OnMouseUp() {
        if (tower.isNearSnapPosition) {
            sphereCollider.enabled = true;
        }
    }

    private void OnTriggerStay(Collider other) {
        if (!tower.isPickedUp && other.transform.tag.Equals("Tower Drop Off Point")) {
            Debug.Log("Drop off point is recongnized");
            // Highlight all possible end nodes that the tower can drop off to
            // Make them clickable


        }
    }

    private void OnTriggerEnter(Collider other) {
        Debug.Log("Trigger clay person");

        if (!tower.isPickedUp && other.transform.tag.Equals("Clay Person")) {
            StartCoroutine(MoveClayPerson(other.transform));
        }
    }

    IEnumerator MoveClayPerson(Transform clayPersonTransform) {
        float timeLeft = 1.0f;

        Rigidbody clayRB = clayPersonTransform.GetComponent<Rigidbody>();
        clayRB.useGravity = false;

        while (timeLeft > 0f) {
            timeLeft -= Time.deltaTime;
            clayPersonTransform.RotateAround(transform.position, Vector3.up, 0.5f);
            yield return new WaitForSeconds(0.01f);
        }

        clayRB.useGravity = true;
    }
}
